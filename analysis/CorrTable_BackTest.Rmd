---
title: "R Notebook"
output: html_notebook
---

```{r message=FALSE, warning=FALSE, paged.print=FALSE}

library(devtools)
install_github("vqv/ggbiplot")
library(ggbiplot)
library(factoextra) # for fviz_eig
library(corrplot)
library(Hmisc)

library(readxl)
library(openxlsx) # for write.xlsx
library(PerformanceAnalytics)
library(xts)
library(lubridate) # to shift months forward or backwards by one
library(zoo) # for replacing NA with last value (na.locf(...)
library(ggplot2)
library(ggthemes) # for more ggplot themes
library(ggpubr) # for ggplot to create multiple plots in one file
library(gridExtra) # to create table

rm(list=ls())
try(dev.off(dev.list()["RStudioGD"]), silent=TRUE) # delete all previous plots
```

Paramters
```{r}
rf = (1 + 0.01) ^(1/52) - 1 # 1 % monthly risk-free
txnC = 0.001 # transaction costs
```

Function
```{r}
#### lets back test
  letsBacktest <- function(weights, returns, txnCosts, shifted = T){
    # shift weights if necessary
    if (shifted == T) {
      weightsCore <- coredata(weights)
      date <- index(weights) %m-% days(1) # shift days by one back
      weights <- xts(weights, order.by = date) #  transformed weights
    }
    
    # compute portfolio returns (without transaction costs)
    r <- Return.portfolio(returns, weights = weights, verbose = T)
    
    # compute turnover
    beginWeights <- r$BOP.Weight
    endWeights <- r$EOP.Weight
    endWeightsLastPeriod <- xts(rbind(rep(0,ncol(endWeights)), coredata(lag(endWeights))[-1,]),
                                order.by = index(endWeights))
    txns <- beginWeights - endWeightsLastPeriod
    periodTurnover <- xts(rowSums(abs(txns/2)), order.by=index(txns))
    
    # incorporating transaction costs into returns
    txnCosts <- periodTurnover * - txnCosts
    returnsWithTxnCosts <- r$returns + txnCosts
    
    # create list as output
    colnames(periodTurnover) <- "Turnover"
    resultsBacktest <- c(list(periodTurnover = periodTurnover, returnsWithTxnCosts = returnsWithTxnCosts), r)
    return(resultsBacktest)
  }

#### getWeights
  getWeights <- function(signal) ifelse(signal > 0, 1, 0) # gives out 1 iff signal is postive otherwise 0
```




#### PCA and correlation table

Correlation table
```{r fig.height=7, fig.width=7}
varStrat1 <- read_excel("market_data_chosen.xlsx", sheet = "inputData") # load data
varStrat1 <- varStrat1[, c("DY", "VOL", "STP", "CF", "MOM_4w", "MOM_52w")] # pick variables

spear <- rcorr(as.matrix(varStrat1), type = "spearman")$r
p.spear <- rcorr(as.matrix(varStrat1), type = "spearman")$P
col.pear <- colorRampPalette(c("#BB4444", "#EE9988", "#FFFFFF", "#4c956c", "#2c6e49"))
col.spear <- colorRampPalette(c("#BB4444", "#EE9988", "#FFFFFF", "#77AADD", "#4477AA"))

corrplot(spear, method="color", col=col.spear(200),  
         type="upper",
         addCoef.col = "black", # Add coefficient of correlation
         tl.col="black", tl.srt=45, #Text label color and rotation
         # Combine with significance
          p.mat = p.spear, sig.level = 0.05, insig = "blank", 
         # hide correlation coefficient on the principal diagonal
         diag=F)

```

PCA - Correlation table
```{r fig.height=8, fig.width=8}
market <- read.csv("market_data_update.csv", sep=";")
market <- market[, c(-1,-22:-27)] # remove date variable & return data
marketPCA <- prcomp(market, center=T, scale=T) # calculate PCA (output: list)

matrix <- cbind(marketPCA$x[,1:4], market)
col <- colorRampPalette(c("#BB4444", "#EE9988", "#FFFFFF", "#77AADD", "#4477AA"))
correl <- cor(matrix)
p.mat <- cor.mtest(matrix)$p

# set p-value of correl btw -0.5 & 0.5 equal to 1 s.t. in plot they will be left out
higherThen <- ifelse(correl > 0.5 | correl < -0.5, 1, NA)
pAndHigher <- higherThen * p.mat
pAndHigher[is.na(pAndHigher)] <- 1

corrplot(correl, method="color", col=col(200),  
         type="upper",
         tl.col="black", tl.srt=45, #Text label color and rotation
         # Combine with significance
         p.mat = pAndHigher, sig.level = 0.01, insig = 'label_sig', 
         # hide correlation coefficient on the principal diagonal
         diag=FALSE)
```





#### Backtest

Load data
```{r}
# load return data
returnData <- read_excel("market_data_chosen.xlsx", sheet = "outputData")
returnsXts <- xts(returnData$`_SPX`, order.by = returnData$Date)
colnames(returnsXts) <- "SPX"

# create signal for first strategy
signal1 <- read_excel("signal_strat1.xlsx", sheet = "Sheet1")
weightsStrat1 <- xts(signal1$`Return Predicted`, order.by = signal1$Date)
weightsStrat1 <- ifelse(weightsStrat1 > 0, 1, 0)
colnames(weightsStrat1) <- "SPX"

# create signal for second strategy
signal2 <- read_excel("signal_strat2.xlsx", sheet = "Sheet1")
weightsStrat2 <- xts(signal2$`Return Predicted`, order.by = signal2$Date)
weightsStrat2 <- ifelse(weightsStrat2 > 0, 1, 0)
colnames(weightsStrat2) <- "SPX"
```

Compute strategy return
```{r fig.height=4, fig.width=8}
stratReturns1 <- letsBacktest(weightsStrat1, returnsXts, txnC)
stratReturns2 <- letsBacktest(weightsStrat2, returnsXts, txnC)

r1 <- cbind(stratReturns1$returnsWithTxnCosts, stratReturns1$returns, returnsXts)
r2 <- cbind(stratReturns2$returnsWithTxnCosts, stratReturns2$returns, returnsXts)
colnames(r1) <- c("TxnCosts", "NoTxnCosts", "SPX")
colnames(r2) <- c("TxnCosts", "NoTxnCosts", "SPX")
startDate1 <- substr(index(weightsStrat1)[1], 1, 10)
startDate2 <- substr(index(weightsStrat2)[1], 1, 10)
r1 <- r1[paste(startDate1, "/", sep="")]
r2 <- r2[paste(startDate2, "/", sep="")]

# SPX vola
SPXvol <- sd(r1$SPX)
Stratvol1 <- sd(r1$NoTxnCosts)
Stratvol2 <- sd(r2$NoTxnCosts)

# adjust vola of SPX
r1$SPX_relev <- Stratvol1 / SPXvol * (r1$SPX - rf) + rf
r2$SPX_relev <- Stratvol2 / SPXvol * (r2$SPX - rf) + rf
```

Performance statistics
```{r}
# create IS and OoS subsets
IS1 <- r1["/2017-10-22"]
OoS1 <- r1["2017-10-22/"]
IS2 <- r2["/2017-10-22"]
OoS2 <- r2["2017-10-22/"]

# IS vs OoS performance
table.AnnualizedReturns(IS1, Rf = rf)
table.AnnualizedReturns(OoS1, Rf = rf)
table.AnnualizedReturns(IS2, Rf = rf)
table.AnnualizedReturns(OoS2, Rf = rf)

# strategy annual turnover 1
sum(stratReturns1$periodTurnover["/2017-10-22"]) / length(stratReturns1$periodTurnover["/2017-10-22"]) * 52
sum(stratReturns1$periodTurnover["2017-10-22/"]) / length(stratReturns1$periodTurnover["2017-10-22/"]) * 52

# strategy annual turnover 2
sum(stratReturns2$periodTurnover["/2017-10-22"]) / length(stratReturns2$periodTurnover["/2017-10-22"]) * 52
sum(stratReturns2$periodTurnover["2017-10-22/"]) / length(stratReturns2$periodTurnover["2017-10-22/"]) * 52
```


Plot strategy 1
```{r fig.height=4, fig.width=10}
cumRetSPX <- cumprod(1 + r1$SPX) -1
cumRetSPX_relev <- cumprod(1 + r1$SPX_relev) - 1
cumRetStrat <- cumprod(1 + r1$TxnCosts) -1
cumRetStratnoC <- cumprod(1 + r1$NoTxnCosts) - 1

signal <- xts(apply(abs(r1$TxnCosts), MARGIN = 1, FUN = getWeights), order.by = index(r1))
signalStart <- index(signal)[which(signal %in% 1)] %m-% weeks(1)
signalEnd <- signalStart %m+% weeks(1)

# merge bars together which are adjacent (only for better aesthetics)
signalStartAdj <- ifelse(signalStart==c(0, signalEnd[-length(signalEnd)]), NA, signalStart)
signalStartAdj <- as.POSIXct(signalStartAdj, origin = as.Date(0))
signalStartAdj <- signalStartAdj[!is.na(signalStartAdj)]
signalEndAdj <- ifelse(signalEnd==c(signalStart[-1],0), NA, signalEnd)
signalEndAdj <- as.POSIXct(signalEndAdj, origin = as.Date(0))
signalEndAdj <- signalEndAdj[!is.na(signalEndAdj)]

p1 <- ggplot()+ geom_line(aes(x = index(cumRetSPX), y =cumRetSPX + 1, col = "SPX"), size=0.9) +
  geom_line(aes(x = index(cumRetStrat), y =cumRetStrat + 1, col = "Strategy 1 with transaction costs"), size=0.9) +
  geom_line(aes(x = index(cumRetSPX_relev), y =cumRetSPX_relev + 1, col = "SPX vola scaled"), size=0.9) +
  geom_line(aes(x = index(cumRetStratnoC), y =cumRetStratnoC + 1, col = "Strategy 1 without transaction costs"),
            size=0.9) +
  geom_rect(aes(xmin=signalStartAdj,xmax=signalEndAdj, ymin=-0, ymax=+Inf),
            fill="black", alpha=0.2) +scale_y_log10() +
  labs(x = "Date", y = "Cummulative Returns", color = "Legend") + theme_calc() +
  theme(axis.text.x = element_text(size=9),
        axis.title=element_text(size=11),
        axis.text.y = element_text(size=9),
        plot.title = element_text(size=15, face="bold", color = "black"), legend.position=c(0.145,0.8)) +
  geom_vline(aes(xintercept = as.integer(as.POSIXct("2017-10-22"))), col = "black", linetype = "longdash") +
   scale_colour_manual(breaks = c("SPX", "SPX vola scaled",
                                  "Strategy 1 without transaction costs", "Strategy 1 with transaction costs"),
                       values = c("#ef476f", "#ffd166", "#06d6a0", "#118ab2"))
p1
```

Plot strategy 2
```{r fig.height=4, fig.width=10}
cumRetSPX <- cumprod(1 + r2$SPX) -1
cumRetSPX_relev <- cumprod(1 + r2$SPX_relev) - 1
cumRetStrat <- cumprod(1 + r2$TxnCosts) -1
cumRetStratnoC <- cumprod(1 + r2$NoTxnCosts) - 1

signal <- xts(apply(abs(r2$TxnCosts), MARGIN = 1, FUN = getWeights), order.by = index(r2))
signalStart <- index(signal)[which(signal %in% 1)] %m-% weeks(1)
signalEnd <- signalStart %m+% weeks(1)

# merge bars together which are adjacent (only for better aesthetics)
signalStartAdj <- ifelse(signalStart==c(0, signalEnd[-length(signalEnd)]), NA, signalStart)
signalStartAdj <- as.POSIXct(signalStartAdj, origin = as.Date(0))
signalStartAdj <- signalStartAdj[!is.na(signalStartAdj)]
signalEndAdj <- ifelse(signalEnd==c(signalStart[-1],0), NA, signalEnd)
signalEndAdj <- as.POSIXct(signalEndAdj, origin = as.Date(0))
signalEndAdj <- signalEndAdj[!is.na(signalEndAdj)]

p2 <- ggplot()+ geom_line(aes(x = index(cumRetSPX), y =cumRetSPX + 1, col = "SPX"), size=0.9) +
  geom_line(aes(x = index(cumRetStrat), y =cumRetStrat + 1, col = "Strategy 2 with transaction costs"), size=0.9) +
  geom_line(aes(x = index(cumRetSPX_relev), y =cumRetSPX_relev + 1, col = "SPX vola scaled"), size=0.9) +
  geom_line(aes(x = index(cumRetStratnoC), y =cumRetStratnoC + 1, col = "Strategy 2 without transaction costs"),
            size=0.9) +
  geom_rect(aes(xmin=signalStartAdj,xmax=signalEndAdj, ymin=-0, ymax=+Inf),
            fill="black", alpha=0.2) +scale_y_log10() +
  labs(x = "Date", y = "Cummulative Returns", color = "Legend") + theme_calc() +
  theme(axis.text.x = element_text(size=9),
        axis.title=element_text(size=11),
        axis.text.y = element_text(size=9),
        plot.title = element_text(size=15, face="bold", color = "black"), legend.position=c(0.145,0.8)) +
  geom_vline(aes(xintercept = as.integer(as.POSIXct("2017-10-22"))), col = "black", linetype = "longdash") +
   scale_colour_manual(breaks = c("SPX", "SPX vola scaled",
                                  "Strategy 2 without transaction costs", "Strategy 2 with transaction costs"),
                       values = c("#ef476f", "#ffd166", "#06d6a0", "#118ab2"))
p2
```
