# Investments: Machine Learning for Finance

## Description

The goal of this project was to discover the predictive ability of AI systems on financial assets. Due to the project requirements it was necessary to predict returns on the S&P 500 index. A timing strategy on such an efficient index is a difficult task in itselfs. Therefore, unsurprisingly our strategy fails out of sample and does not show any statistical significance. Nevertheless, training our stacked neural network (LSTM) on training data we achieve good results of sharpe ratios of up to 1. 

In the paper we investigate different AI models, the correlation between possible predictive variables. Here we especially focused on using parameters with forward looking predictive ability such as Momentum, VIX, Slope of the Yield curve etc. We then trained a model based on these parameters and tried to identify trends in the S&P. To do so we smoothed the returns of the S&P with an Autoencoder model first. This gave as good results. In the end we developed a trading strategy based on our signal which unfortunately fails to deliver profits after trading costs. 

This research was conducted in the scope of the AI: Investment subject at UZH and in cooperation with LGT Private Bank.

## This short Introduction serves as a guideline thorugh the code

## Step 1
One has to download all python dependencies which are listed here:

* tensorflow
* pandas
* numpy
* matplotlib
* sklearn


## Step 2
The developer selects the variable signals and target to predict in the main file
He further needs to specify whether the LSTM should be trained on the smoothed signals/targets or on the real world data

## Step 3
One needs to analyze the graphs. For that separate code (analysis folder) is used.
