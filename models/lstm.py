import tensorflow as tf
from keras import Sequential
from keras.layers import LSTM, Dropout, Dense


class LSTMReturnPredictor(tf.keras.Model):

    def __init__(self, input_shape, training=False, probability=False, dropout=0.2):
        super().__init__()
        self.lstm1 = tf.keras.layers.LSTM(units=5, input_shape=input_shape, return_sequences=True, use_bias=True)
        self.dropout = tf.keras.layers.Dropout(dropout)
        self.lstm2 = tf.keras.layers.LSTM(units=10, use_bias=True)
        self.lstm3 = tf.keras.layers.LSTM(units=5)
        self.dense = tf.keras.layers.Dense(units=1, activation = 'tanh')
        self.dense_sigmoid = tf.keras.layers.Dense(units=1, activation='sigmoid')


        self.probability = probability
        self.training=training


    def call(self, inputs):
        print(inputs.shape)
        x = self.lstm1(inputs)
        print(x.shape)
        x = self.apply_dropout(x)
        print(x.shape)

        x = self.lstm2(x)
        x = self.apply_dropout(x)
        # x = tf.nn.relu(x)
        #
        # # x = self.lstm2(x)
        # # x = self.apply_dropout(x)
        #
        # x = self.lstm3(x)
        # x = self.apply_dropout(x)
        # x = tf.nn.relu(x)

        if self.probability:
            print("use sigmoid")
            final_x = self.dense_sigmoid(x)
        else:
            final_x = self.dense(x)


        return final_x


    def apply_dropout(self, x):
        if self.training:
            x = self.dropout(x, training=self.training)

        return x


    def change_to_prod(self):
        self.training = False