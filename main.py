import random
from signals.predict_and_trade import main_predict
from signals.preprocess import preprocess

if __name__ == '__main__':
    # set variables
    signals = ["VOL", "DY", "mov", "CPI"]
    targets = '_SPX'
    # signals = ["DY", "VOL", "STP", "CF", "MOM_4w", "MOM_52w"]
    all_columns = signals.copy()
    all_columns.append(targets)

    # run autoencoder
    preprocess(signals, targets)


    random.seed(10)
    main_predict(signals, targets, all_columns)