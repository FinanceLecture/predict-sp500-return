import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from matplotlib.dates import DateFormatter
from matplotlib.ticker import FormatStrFormatter
from sklearn import preprocessing

from models.lstm import LSTMReturnPredictor


class WindowGenerator():

  def __init__(self, input_width, label_width, shift, train_df, input_columns=None, label_columns=None):
    # Work out the label column indices.
    self.label_columns = label_columns
    if label_columns is not None:
      self.label_columns_indices = {name: i for i, name in enumerate(label_columns)}
    self.train_label_indices = {name: i for i, name in enumerate(train_df.columns)}

    # ...and the input column indices
    self.input_columns = input_columns
    if input_columns is not None:
      self.input_columns_indices = {name: i for i, name in enumerate(input_columns)}
    self.train_input_indices = {name: i for i, name in enumerate(train_df.columns)}

    # Work out the window parameters.
    self.input_width = input_width
    self.label_width = label_width
    self.shift = shift

    self.total_window_size = input_width + shift

    self.input_slice = slice(0, input_width)
    self.input_indices = np.arange(self.total_window_size)[self.input_slice]

    self.label_start = self.total_window_size - self.label_width
    self.labels_slice = slice(self.label_start, None)
    self.label_indices = np.arange(self.total_window_size)[self.labels_slice]

  def split_window(self, features):
      inputs = features[:, self.input_slice, :]
      labels = features[:, self.labels_slice, :]
      if self.input_columns is not None:
        inputs = tf.stack([inputs[:, :, self.train_input_indices[name]] for name in self.input_columns], axis=-1)
      if self.label_columns is not None:
        labels = tf.stack([labels[:, :, self.train_label_indices[name]] for name in self.label_columns], axis=-1)
      return inputs, labels

  def make_dataset(self, data, shuffle = False, batchsize = 500,):
      data = np.array(data, dtype=np.float32)
      ds = tf.keras.preprocessing.timeseries_dataset_from_array(data=data, targets=None, sequence_length=self.total_window_size,
                                                                sequence_stride=1, sampling_rate=1, shuffle=shuffle, batch_size=batchsize)
      ds = ds.map(self.split_window)
      return ds


def train_model(model, window, train_df, loss=tf.losses.MeanSquaredError(), metrics=tf.metrics.MeanSquaredError(), epochs=200):
    td = window.make_dataset(train_df, batchsize=150, shuffle=True)

    # cross-validation
    train_data = td.take(5)
    val_data = td.skip(5)


    # lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(0.01, decay_steps=150, decay_rate=0.95, staircase=True)
    # use adam instead of sgd
    model.compile(loss=loss, optimizer='adam',
                  metrics=[metrics])
    model.run_eagerly = False
    early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=100, mode='min')
    #750
    history = model.fit(train_data, validation_data=val_data, epochs=epochs,
                        batch_size=150, callbacks=[early_stopping])
    model.summary()

    return history, model


def plot_return_graphs(model, eval_train, eval_test, train_df, test_df, test_df1, train_df1, lb, lf, targets, history):
    fig, axs = plt.subplots()
    # axs.xaxis.set_major_formatter(DateFormatter('%Y'))
    axs.plot(history.history['loss'])
    axs.plot(history.history['val_loss'])
    axs.legend(['training loss', 'validation loss'])

    # CHECK IS and OS performance and P/L of a trading strategy
    plt.figure()
    plt.subplot(221)
    y_pred = model.predict(eval_train)
    y_true = np.concatenate([y for x, y in eval_train], axis=0)
    mse = tf.reduce_mean(tf.keras.losses.MSE(y_true, y_pred))
    plt.plot(train_df.index[lb + lf - 1:], y_true[:, -1, -1])
    plt.plot(train_df.index[lb + lf - 1:], y_pred[:, -1], '--')
    plt.xticks(rotation=45)
    plt.title('in-sample mse =%1.2f' % mse)
    plt.legend(['y_true', 'y_pred'])

    plt.subplot(222)
    y_mkt = train_df1.iloc[lb + lf - 1:, :].loc[:, targets]
    # position taking: simple switch
    pos = np.sign(np.squeeze(y_pred[:, -1]))
    pos[pos == -1] = 0
    pnl = pos[1:] * y_mkt[:-1]
    pnl2 = pos[2:] * y_mkt[:-2]
    plt.plot(y_mkt.index[:-1], np.cumsum(pnl))
    plt.plot(y_mkt.index[:-2], np.cumsum(pnl2), '--')
    plt.plot(y_mkt.index[:-1], np.cumsum(y_mkt[:-1]))
    sr = pnl.mean() / pnl.std() * np.sqrt(52)
    plt.title('in-sample Sharpe ratio = %1.2f' % sr)
    plt.xticks(rotation=45)
    plt.legend(['pnl [t+1]', 'pnl [t+2]', 'underlying'])

    plt.subplot(223)
    y_pred = model.predict(eval_test)
    y_true = np.concatenate([y for x, y in eval_test], axis=0)
    mse = tf.reduce_mean(tf.keras.losses.MSE(y_true, y_pred))
    plt.plot(test_df.index[lb + lf - 1:], y_true[:, -1, -1])
    plt.plot(test_df.index[lb + lf - 1:], y_pred[:, -1], '--')
    plt.title('out-of-sample mse =%1.2f' % mse)
    plt.xticks(rotation=45)
    plt.legend(['y_true', 'y_pred'])

    plt.subplot(224)
    y_mkt = test_df1.iloc[lb + lf - 1:, :].loc[:, targets]
    # position taking: simple switch
    pos = np.sign(np.squeeze(y_pred[:, -1]))
    pos[pos == -1] = 0
    pnl = pos[1:] * y_mkt[:-1]
    pnl2 = pos[2:] * y_mkt[:-2]
    plt.plot(y_mkt.index[:-1], np.cumsum(pnl))
    plt.plot(y_mkt.index[:-2], np.cumsum(pnl2), '--')
    plt.plot(y_mkt.index[:-1], np.cumsum(y_mkt[:-1]))
    sr = pnl.mean() / pnl.std() * np.sqrt(52)
    plt.title('out-of-sample Sharpe ratio = %1.2f' % sr)
    plt.legend(['pnl [t+1]', 'pnl [t+2]', 'underlying'])
    plt.xticks(rotation=45)
    plt.tight_layout()
    plt.show()


def main_predict(signals, targets, all_columns):
    # set seed for reproducability
    tf.random.set_seed(1234)

    # signals = ["DY", "VOL", "STP", "CF", "MOM_4w", "MOM_52w"]
    # all_columns = ["DY", "VOL", "STP", "CF", "MOM_4w", "MOM_52w", '_SPX']


    new_col = []
    signals_smoothed = []
    targets_smoothed = f'{targets}_smoothed'
    for col in all_columns:
        new_col.append(f'{col}_smoothed')
        signals_smoothed.append(f'{col}_smoothed')


    new_col.extend(all_columns)

    # PREPARE DATA
    df = pd.read_excel('./data/data_preprocessed.xlsx')
    df = df.set_index(df.Date)
    df0 = df.drop(columns = 'Date')
    # drop null rows (due to lookback period of autoencoder)
    df1 = df0.dropna()
    df1.set_axis(new_col, axis=1, inplace=True)
    n = len(df1)

    # get business test data set
    business_set = df1[int(0.8*n):]
    technical_set = df1[0:int(0.8*n)]

    # hold out train/test data
    n_technical = len(technical_set)
    train_df1 = technical_set[0:int(0.8*n_technical)]
    test_df1 = technical_set[int(0.8*n_technical):]
    # initialize standardscaler
    mm_scaler = preprocessing.StandardScaler()
    # normalize training set (fit only on train)
    train_dfm = mm_scaler.fit_transform(train_df1)
    test_dfm = mm_scaler.transform(test_df1)
    # convert back to pandas dataframe
    train_df = pd.DataFrame(train_dfm, index=train_df1.index, columns=train_df1.columns)
    test_df = pd.DataFrame(test_dfm, index=test_df1.index, columns=test_df1.columns)
    train_df['positive_binary'] = np.where(train_df[targets] >= 0, 1, 0)
    train_df['negative_binary'] = np.where(train_df[targets] < 0, 1, 0)
    test_df['positive_binary'] = np.where(test_df[targets] >= 0, 1, 0)
    test_df['negative_binary'] = np.where(test_df[targets] < 0, 1, 0)

    # define sliding window
    lf = 1      # look forward
    ks = 5      # kernel size
    lw = 1      # label width
    lb = 15
    shift = 1
    useCNN = False
    # look back with shift of one to predict the next return
    window_return = WindowGenerator(input_width=lb, label_width=1, shift=shift, train_df=train_df, input_columns=signals, label_columns=[targets])
    window_return_smoothed = WindowGenerator(input_width=lb, label_width=1, shift=shift, train_df=train_df ,input_columns=signals, label_columns=[targets])
    window_pos_prob = WindowGenerator(input_width=lb, label_width=1, shift=shift, train_df=train_df , input_columns=signals,
                                    label_columns=['positive_binary'])
    window_neg_prob = WindowGenerator(input_width=lb, label_width=1, shift=shift, train_df=train_df , input_columns=signals,
                                      label_columns=['negative_binary'])


    # # SET-UP AND TRAIN MODEL
    # model = tf.keras.Sequential()
    # # Version 1: Convolutional Network
    # model.add(tf.keras.layers.Conv1D(filters=4, kernel_size=ks, activation='relu', use_bias=False))
    # model.add(tf.keras.layers.MaxPooling1D(pool_size=4, strides=1, padding='same'))
    # model.add(tf.keras.layers.Conv1D(filters=32, kernel_size=ks, activation='relu', use_bias=False))
    # model.add(tf.keras.layers.MaxPooling1D(pool_size=4, strides=1, padding='same'))
    # model.add(tf.keras.layers.Conv1D(filters=4, kernel_size=lb-2*(ks-1)-(lw-1), activation='relu', use_bias=False))
    # model.add(tf.keras.layers.Dense(units=1))

    # # Version 2: Recurrent Network
    # model.add(tf.keras.layers.SimpleRNN(2, return_sequences=False, return_state=False, activation='tanh', use_bias=True))
    # model.add(tf.keras.layers.Dense(1, activation='tanh', use_bias=True))

    return_model = LSTMReturnPredictor((shift + lb, len(df.columns) /2), True, False)
    return_model_smoothed = LSTMReturnPredictor((shift + lb, len(df.columns) /2), True, False)
    pos_prob_model = LSTMReturnPredictor((shift + lb, len(df.columns)/2), True, True)
    neg_prob_model = LSTMReturnPredictor((shift + lb, len(df.columns)/2), True, True)

    history, model = train_model(return_model, window_return, train_df, epochs=500)
    history_smoothed, model_smoothed = train_model(return_model_smoothed, window_return_smoothed, train_df, epochs=500)
    history_pos_prob, model_pos_prob = train_model(pos_prob_model, window_pos_prob, train_df, loss="binary_crossentropy", metrics='accuracy', epochs=100)
    history_neg_prob, model_neg_prob = train_model(neg_prob_model, window_neg_prob, train_df, loss="binary_crossentropy", metrics='accuracy', epochs= 100)

    # all
    df_total = mm_scaler.transform(df1)
    df_total = pd.DataFrame(df_total, index=df1.index, columns=df1.columns)
    df_total['Date'] = df['Date']
    df_total['negative_binary'] = np.where(df_total[targets] < 0, 1, 0)
    df_total['positive_binary'] = np.where(df_total[targets] >= 0, 1, 0)
    df_total.set_index('Date')
    df_total.drop(columns = 'Date', inplace=True)
    df_total_return = window_return.make_dataset(df_total, batchsize=df_total.shape[0], shuffle=False)
    df_total_return_smoothed = window_return_smoothed.make_dataset(df_total, batchsize=df_total.shape[0], shuffle=False)
    df_total_pos = window_pos_prob.make_dataset(df_total, batchsize=df_total.shape[0], shuffle=False)
    df_total_neg = window_neg_prob.make_dataset(df_total, batchsize=df_total.shape[0], shuffle=False)
    model.training=False
    model_neg_prob.traing=False
    model_pos_prob.training=False
    pred_total_return = np.squeeze(model.predict(df_total_return)[:, -1])
    pred_total_return_smoothed = np.squeeze(model_smoothed.predict(df_total_return_smoothed)[:, -1])
    pred_pos = np.squeeze(model_pos_prob.predict(df_total_pos)[:, -1])
    pred_neg = np.squeeze(model_neg_prob.predict(df_total_neg)[:, -1])

    df1 = df1.iloc[lb - 1 + shift:, :]
    df1["Return Predicted"] = pred_total_return
    df1["Return Predicted Smoothed"] = pred_total_return_smoothed
    df1["Positive Prob"] = pred_pos
    df1["Negative Prob"] = pred_neg

    df1.to_excel('./data/signal.xlsx')


    eval_train = window_return.make_dataset(train_df, batchsize=train_df.shape[0], shuffle=False)
    eval_test = window_return.make_dataset(test_df, batchsize=test_df.shape[0], shuffle=False)
    plot_return_graphs(model, eval_train, eval_test, train_df, test_df, test_df1, train_df1, lb, lf, targets, history)

    eval_train_smoothed = window_return_smoothed.make_dataset(train_df, batchsize=train_df.shape[0], shuffle=False)
    eval_test_smoothed = window_return_smoothed.make_dataset(test_df, batchsize=test_df.shape[0], shuffle=False)
    plot_return_graphs(model_smoothed, eval_train_smoothed, eval_test_smoothed, train_df, test_df, test_df1, train_df1, lb, lf, targets, history_smoothed)



